
# Agile and Scaling
##### A Brief Introduction

---

# Agile Manifesto

+++

###Individuals and Interactions
##### over processes and tools
<br>
###Working Software
##### over comprehensive documentation
<br>
###Customer Collaboration
##### over contract negotiation
<br>
###Responding to Change
##### over following a plan

---

###Agile for Individual Teams
<br>
* Scrum
* Kanban
* Scrumban
* etc.

---

###Why do we need Scaling?
<br>
* Co-ordination
* Team Maturity and Preference
* Cross Cutting Teams

---

###Scaled Agile Frameworks
<br>
* SAFe - Scaled Agile Framework
* DAD - Disciplined Agile Delivery
* LeSS - Large Scale Scrum

---

#Tool Support
<br>
* JIRA Agile
* VersionOne

---

#Demo